# -*- coding: utf-8 -*-


"""
Extended kalman filter (EKF) localization sample
author: Atsushi Sakai (@Atsushi_twi)
from: https://pythonrobotics.readthedocs.io/en/latest/modules/localization.html#extended-kalman-filter-localization
https://github.com/AtsushiSakai/PythonRobotics/blob/916b4382de090de29f54538b356cef1c811aacce/Localization/extended_kalman_filter/extended_kalman_filter.py

Version4 : CORREGIDOS: matrices de cov Qm y Rm (estaban invertidas) . 
           PEND:"colocar datos GPS sin vector de control"   y preguntas "???"
           HACER: captutar los datosGPS de measurements usando mi metodosplot-pandas y  
                 introducir GPSdata, R,   OdometryDatos, Q                                
                               
           --> y luego paraslo a un notebookJupyter
                 
"""

import numpy as np
import math
import matplotlib.pyplot as plt

#OJO: Las variables globales se ven en el variable explorer

#Datos del GPS: de:  /home/fico/Python-codes/kf.py
#measurements = np.asarray([(399,293),(403,299),(409,308),(416,315),(418,318),(420,323),(429,326),(423,328),(429,334),(431,337),(433,342),(434,352),(434,349),(433,350),(431,350),(430,349),(428,347),(427,345),(425,341),(429,338),(431,328),(410,313),(406,306),(402,299),(397,291),(391,294),(376,270),(372,272),(351,248),(336,244),(327,236),(307,220)])

# Estimation parameter of EKF
Qm = np.diag([0.1, 0.1, np.deg2rad(1.0), 1.0])**2  # predict state covariance Q   -> #Precision model system (i.e.: odometry) - como se deduce??     -> como se modela este error???    
Rm = np.diag([1.0, 1.0])**2             # Observation x,y position covariance R   -> #Precision obseravation system (i.e.: GPS)-ver datasheet GPS

#  Simulation parameter
Qsim = np.diag([1.0, np.deg2rad(30.0)])**2  #para agregar ruido al proceso F  
Rsim = np.diag([0.5, 0.5])**2               #para agregar ruido a la observacion H 

DT = 0.1  # time tick [s]
#SIM_TIME = 50.0  # simulation time [s]
SIM_TIME = 2.0  # simulation time [s]

show_animation = True

#Esta es la funcion de control que fuerza el movimiento
def calc_input():
    v = 1.0  # [m/s]           Velocidad lineal v
    yawrate = 0.1  # [rad/s]   Velocaid angular w
    u = np.array([[v, yawrate]]).T
    return u


# Aqui va el groudTruth(truepath), la lectura del sensor(i.e.GPS)  
def observation(xTrue, xd, u):
#ojo:#z  green: observation (ex. GPS) ----z:  observacion ruidosa
     #x  True blue:true path       -- usando vector de estado "xTRrue"y control"u" puros (sin ruidos)    
     #xd black: odometry dead-dead reckoning trajectory  -- recibe xDR(odometry) y genera odometry ruidosa 
     #u  red: EKF                   --genera ud:  vector de control ruidoso

#blue:true path
    xTrue = motion_model(xTrue, u)

#green: observation (ex. GPS)            # add noise to gps x-y
    zx = xTrue[0, 0] + np.random.randn() * Rsim[0, 0]#agrega ruido al modelo H   osea Rmatrix
    zy = xTrue[1, 0] + np.random.randn() * Rsim[1, 1]
    z = np.array([[zx, zy]])


    # add noise to input
    ud1 = u[0, 0] + np.random.randn() * Qsim[0, 0]#agrega ruido al modelo F   osea Qmatrix
    ud2 = u[1, 0] + np.random.randn() * Qsim[1, 1]
    ud = np.array([[ud1, ud2]]).T

    xd = motion_model(xd, ud)
    

    return xTrue, z, xd, ud
# retorna  xTrue ,  z(gps)  , xd( XDR odometry) ,  ud 


#blue:true path  #asume velocidad contante ? si v,W en calc_input()
def motion_model(x, u):

    F = np.array([[1.0, 0, 0, 0],
                  [0, 1.0, 0, 0],
                  [0, 0, 1.0, 0],
                  [0, 0, 0, 0]])

    B = np.array([[DT * math.cos(x[2, 0]), 0],
                  [DT * math.sin(x[2, 0]), 0],
                  [0.0, DT],
                  [1.0, 0.0]])
#    #temp DEBUG
#    print("F : ")
#    print(F)
#    print("x : ")
#    print(x)
#    print("F * x : ")  
#    print(F * x)      # Vuelve al vector x una matriz cuadrada  / ojo:    dot(F,x) es producto escalar , 
#    print("F.dot(x) : ")
#    print(F.dot(x))   #F.dot(x) es matrix*vector    ver= https://stackoverflow.com/questions/42517281/difference-between-numpy-dot-and-a-dotb
#    print("")

    x = F.dot(x) + B.dot(u) 
    return x


def observation_model(x):
    #  Observation Model
    H = np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0]
    ])

    z = H.dot(x)

    return z


def jacobF(x, u):
    """
    Jacobian of Motion Model
    motion model
    x_{t+1} = x_t+v*dt*cos(yaw)
    y_{t+1} = y_t+v*dt*sin(yaw)
    yaw_{t+1} = yaw_t+omega*dt
    v_{t+1} = v{t}
    so
    dx/dyaw = -v*dt*sin(yaw)
    dx/dv = dt*cos(yaw)
    dy/dyaw = v*dt*cos(yaw)
    dy/dv = dt*sin(yaw)
    """
    yaw = x[2, 0]
    v = u[0, 0]
    jF = np.array([
        [1.0, 0.0, -DT * v * math.sin(yaw), DT * math.cos(yaw)],
        [0.0, 1.0, DT * v * math.cos(yaw), DT * math.sin(yaw)],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0]])

    return jF


def jacobH(x):
    # Jacobian of Observation Model
    jH = np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0]
    ])

    return jH


def ekf_estimation(xEst, PEst, z, u):

    #  Predict
    xPred = motion_model(xEst, u)
    jF = jacobF(xPred, u)
    PPred = jF.dot(PEst).dot(jF.T) + Qm

    #  Update
    jH = jacobH(xPred)
    zPred = observation_model(xPred)
    y = z.T - zPred
    S = jH.dot(PPred).dot(jH.T) + Rm
    K = PPred.dot(jH.T).dot(np.linalg.inv(S))
    xEst = xPred + K.dot(y)
    PEst = (np.eye(len(xEst)) - K.dot(jH)).dot(PPred)

    return xEst, PEst


def plot_covariance_ellipse(xEst, PEst):
    Pxy = PEst[0:2, 0:2]
    eigval, eigvec = np.linalg.eig(Pxy)

    if eigval[0] >= eigval[1]:
        bigind = 0
        smallind = 1
    else:
        bigind = 1
        smallind = 0

    t = np.arange(0, 2 * math.pi + 0.1, 0.1)
    a = math.sqrt(eigval[bigind])
    b = math.sqrt(eigval[smallind])
    x = [a * math.cos(it) for it in t]
    y = [b * math.sin(it) for it in t]
    angle = math.atan2(eigvec[bigind, 1], eigvec[bigind, 0])
    Qm = np.array([[math.cos(angle), math.sin(angle)],
                  [-math.sin(angle), math.cos(angle)]])
    fx = Qm.dot(np.array([[x, y]]))
    px = np.array(fx[0, :] + xEst[0, 0]).flatten()
    py = np.array(fx[1, :] + xEst[1, 0]).flatten()
    plt.plot(px, py, "--r")


def main():
    print(__file__ + " start!!")

    time = 0.0

    # State Vector [x y yaw v]'
    xEst = np.zeros((4, 1))
    xTrue = np.zeros((4, 1))                       #print (xTrue)
    PEst = np.eye(4)
    xDR = np.zeros((4, 1))  # Dead reckoning - Odometry

    # history
    hxEst = xEst
    hxTrue = xTrue
    hxDR = xTrue
    hz = np.zeros((1, 2))

    while SIM_TIME >= time:
        time += DT
        #funcion de control   --> influye en  observation() y ekf_estimation()
        u = calc_input()

        xTrue, z, xDR, ud = observation(xTrue, xDR, u)
        # retorna  xTrue ,  z(gps)  , xDR(odometry) ,  ud  
        #print (xTrue)

        xEst, PEst = ekf_estimation(xEst, PEst, z, ud)

        # store data history
        hxEst = np.hstack((hxEst, xEst))
        hxDR = np.hstack((hxDR, xDR))
        hxTrue = np.hstack((hxTrue, xTrue))
        hz = np.vstack((hz, z)) # vstack   concatena 2 vectores para volverlos una sola matriz

        if show_animation:
            plt.cla()
            plt.plot(hz[:, 0], hz[:, 1], ".g")#green: observation (ex. GPS)
            plt.plot(hxTrue[0, :].flatten(),# un vector vertical lo vuelve horizontal y una matriz lo vuelve vector   ver: https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.flatten.html
                     hxTrue[1, :].flatten(), "-b") #brue:true path 
            plt.plot(hxDR[0, :].flatten(),
                     hxDR[1, :].flatten(), "-k")#black: odometry dead-dead reckoning trajectory
            plt.plot(hxEst[0, :].flatten(),
                     hxEst[1, :].flatten(), "-r")#red: EKF
            plot_covariance_ellipse(xEst, PEst)
            plt.axis("equal")
            plt.grid(True)
            plt.pause(0.001)


if __name__ == '__main__':
    main()